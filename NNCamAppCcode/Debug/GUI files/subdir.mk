################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../GUI\ files/font12.c \
../GUI\ files/font16.c \
../GUI\ files/font20.c \
../GUI\ files/font24.c \
../GUI\ files/font8.c \
../GUI\ files/ov9655.c \
../GUI\ files/stm32746g_discovery.c \
../GUI\ files/stm32746g_discovery_camera.c \
../GUI\ files/stm32746g_discovery_lcd.c \
../GUI\ files/stm32746g_discovery_sdram.c 

OBJS += \
./GUI\ files/font12.o \
./GUI\ files/font16.o \
./GUI\ files/font20.o \
./GUI\ files/font24.o \
./GUI\ files/font8.o \
./GUI\ files/ov9655.o \
./GUI\ files/stm32746g_discovery.o \
./GUI\ files/stm32746g_discovery_camera.o \
./GUI\ files/stm32746g_discovery_lcd.o \
./GUI\ files/stm32746g_discovery_sdram.o 

C_DEPS += \
./GUI\ files/font12.d \
./GUI\ files/font16.d \
./GUI\ files/font20.d \
./GUI\ files/font24.d \
./GUI\ files/font8.d \
./GUI\ files/ov9655.d \
./GUI\ files/stm32746g_discovery.d \
./GUI\ files/stm32746g_discovery_camera.d \
./GUI\ files/stm32746g_discovery_lcd.d \
./GUI\ files/stm32746g_discovery_sdram.d 


# Each subdirectory must supply rules for building sources it contributes
GUI\ files/font12.o: ../GUI\ files/font12.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F746xx -I"C:/Users/SUSE/Desktop/NNCamApp/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc/Legacy" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Device/ST/STM32F7xx/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/GUI files" -I"C:/Users/SUSE/Desktop/NNCamApp/m7"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"GUI files/font12.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

GUI\ files/font16.o: ../GUI\ files/font16.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F746xx -I"C:/Users/SUSE/Desktop/NNCamApp/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc/Legacy" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Device/ST/STM32F7xx/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/GUI files" -I"C:/Users/SUSE/Desktop/NNCamApp/m7"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"GUI files/font16.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

GUI\ files/font20.o: ../GUI\ files/font20.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F746xx -I"C:/Users/SUSE/Desktop/NNCamApp/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc/Legacy" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Device/ST/STM32F7xx/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/GUI files" -I"C:/Users/SUSE/Desktop/NNCamApp/m7"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"GUI files/font20.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

GUI\ files/font24.o: ../GUI\ files/font24.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F746xx -I"C:/Users/SUSE/Desktop/NNCamApp/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc/Legacy" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Device/ST/STM32F7xx/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/GUI files" -I"C:/Users/SUSE/Desktop/NNCamApp/m7"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"GUI files/font24.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

GUI\ files/font8.o: ../GUI\ files/font8.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F746xx -I"C:/Users/SUSE/Desktop/NNCamApp/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc/Legacy" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Device/ST/STM32F7xx/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/GUI files" -I"C:/Users/SUSE/Desktop/NNCamApp/m7"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"GUI files/font8.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

GUI\ files/ov9655.o: ../GUI\ files/ov9655.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F746xx -I"C:/Users/SUSE/Desktop/NNCamApp/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc/Legacy" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Device/ST/STM32F7xx/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/GUI files" -I"C:/Users/SUSE/Desktop/NNCamApp/m7"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"GUI files/ov9655.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

GUI\ files/stm32746g_discovery.o: ../GUI\ files/stm32746g_discovery.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F746xx -I"C:/Users/SUSE/Desktop/NNCamApp/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc/Legacy" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Device/ST/STM32F7xx/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/GUI files" -I"C:/Users/SUSE/Desktop/NNCamApp/m7"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"GUI files/stm32746g_discovery.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

GUI\ files/stm32746g_discovery_camera.o: ../GUI\ files/stm32746g_discovery_camera.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F746xx -I"C:/Users/SUSE/Desktop/NNCamApp/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc/Legacy" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Device/ST/STM32F7xx/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/GUI files" -I"C:/Users/SUSE/Desktop/NNCamApp/m7"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"GUI files/stm32746g_discovery_camera.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

GUI\ files/stm32746g_discovery_lcd.o: ../GUI\ files/stm32746g_discovery_lcd.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F746xx -I"C:/Users/SUSE/Desktop/NNCamApp/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc/Legacy" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Device/ST/STM32F7xx/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/GUI files" -I"C:/Users/SUSE/Desktop/NNCamApp/m7"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"GUI files/stm32746g_discovery_lcd.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

GUI\ files/stm32746g_discovery_sdram.o: ../GUI\ files/stm32746g_discovery_sdram.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F746xx -I"C:/Users/SUSE/Desktop/NNCamApp/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc/Legacy" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Device/ST/STM32F7xx/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/GUI files" -I"C:/Users/SUSE/Desktop/NNCamApp/m7"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"GUI files/stm32746g_discovery_sdram.d" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


