################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/NN/SoftmaxFunctions/arm_softmax_q15.c \
../Src/NN/SoftmaxFunctions/arm_softmax_q7.c 

OBJS += \
./Src/NN/SoftmaxFunctions/arm_softmax_q15.o \
./Src/NN/SoftmaxFunctions/arm_softmax_q7.o 

C_DEPS += \
./Src/NN/SoftmaxFunctions/arm_softmax_q15.d \
./Src/NN/SoftmaxFunctions/arm_softmax_q7.d 


# Each subdirectory must supply rules for building sources it contributes
Src/NN/SoftmaxFunctions/%.o: ../Src/NN/SoftmaxFunctions/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F746xx -I"C:/Users/SUSE/Desktop/NNCamApp/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/STM32F7xx_HAL_Driver/Inc/Legacy" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/SUSE/Desktop/NNCamApp/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7/r0p1" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Device/ST/STM32F7xx/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/Drivers/CMSIS/Include" -I"C:/Users/SUSE/Desktop/NNCamApp/GUI files" -I"C:/Users/SUSE/Desktop/NNCamApp/m7"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


