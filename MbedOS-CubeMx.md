```
```

#### UPDATE ON Mbed OS Convertion into SW4STMM32

---

This is an update on   merging of  the  project  implemeneted using mbedOS to CubeMX so that it can be  compiled using  SW4STM32 or KeilUVision IDE. The 

idea was to extract   the necessary files from  the  already generated  project and  convert it into the format that will make it compilable uisng SW4STM32.

However,  there    were  issues that I  encountered during compilation with of all necessary libraries that were associated with the project.

After extensive research I would say the problem was as a result of files dependency issue  because for the code to work the compiler needs to read caffe files 

that were converted into C++. There are different CMSIS files modified by ARM and the one used can be found here  [CMSIS_NN](https://github.com/ARM-software/CMSIS_5.git).

There is another that can be generated via keilv5 manage runtime environment. My point is that CMSIS NN is not stable yet  and I think there is high 

flexibility it leverages on when used in a linux enviroment combined with mbed client. This is just what I think.  Also, caffe is only supported on linux. I know it

should work once all necessay files have been generated like what we have. However, I will keep researching on how this can be solved.

---

**_Here is my suggestion:_** I think we should leverage on the method that is working for now while I keep digging deep on how the problem can be solved

as soon as possible if it warrants I contact ARM while working in parallel with other tasks. 

I can set up a laptop up with all dependencies installed and would make it such that compiling the code can be down with a line of script for the sake of any

demo you might want to run for other boards or parties.

---

```
My next shift will be coming weekend and I would keep updating this space on any progress I make. I can share the code that I have developed if there is a need for it.

both locally and using mbed cloud while trying to get where the problem lies. 

```

```
```

---

```
```
