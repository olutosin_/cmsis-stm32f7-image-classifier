################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/NN/ActivationFunctions/arm_nn_activations_q15.c \
../src/NN/ActivationFunctions/arm_nn_activations_q7.c \
../src/NN/ActivationFunctions/arm_relu_q15.c \
../src/NN/ActivationFunctions/arm_relu_q7.c 

OBJS += \
./src/NN/ActivationFunctions/arm_nn_activations_q15.o \
./src/NN/ActivationFunctions/arm_nn_activations_q7.o \
./src/NN/ActivationFunctions/arm_relu_q15.o \
./src/NN/ActivationFunctions/arm_relu_q7.o 

C_DEPS += \
./src/NN/ActivationFunctions/arm_nn_activations_q15.d \
./src/NN/ActivationFunctions/arm_nn_activations_q7.d \
./src/NN/ActivationFunctions/arm_relu_q15.d \
./src/NN/ActivationFunctions/arm_relu_q7.d 


# Each subdirectory must supply rules for building sources it contributes
src/NN/ActivationFunctions/%.o: ../src/NN/ActivationFunctions/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -DSTM32 -DSTM32F7 -DSTM32F746NGHx -DSTM32F746G_DISCO -DDEBUG -DSTM32F746xx -DUSE_HAL_DRIVER -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/CMSIS/core" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/CMSIS/device" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/HAL_Driver/Inc/Legacy" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/HAL_Driver/Inc" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/adv7533" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/ampire480272" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/ampire640480" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/Common" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/exc7200" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/ft5336" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/ft6x06" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/mfxstm32l152" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/mx25l512" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/n25q128a" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/n25q512a" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/otm8009a" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/ov9655" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/rk043fn48h" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/s5k5cag" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/st7735" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/st7789h2" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/stmpe811" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/ts3510" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Components/wm8994" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Fonts" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/Log" -I"C:/Users/SUSE/Desktop/Project/stm32f746g-disco_hal_lib/Utilities/STM32746G-Discovery" -I"C:/Users/SUSE/Desktop/Project/Test/inc" -I"C:/Users/SUSE/Desktop/Project/Test/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"C:/Users/SUSE/Desktop/Project/Test/Middlewares/Third_Party/FreeRTOS/Source/include" -I"C:/Users/SUSE/Desktop/Project/Test/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


