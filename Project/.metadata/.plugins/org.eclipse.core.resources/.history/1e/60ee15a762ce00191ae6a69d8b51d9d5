
/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx.h"
#include "stm32746g_discovery.h"
#include "stm32746g_discovery_camera.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32f7xx_hal_dcmi.h"
#include "LCD_DISCO_F746NG.h"
#include "stdio.h"
#include "LCD_DISCO_F746NG.h"
#include "string.h"

extern "C" {
  //defined in stm32746g_discovery_camera.c
  extern DCMI_HandleTypeDef hDcmiHandler;
  void DCMI_IRQHandler(void) {
      HAL_DCMI_IRQHandler(&hDcmiHandler);
  }
  void DMA2_Stream1_IRQHandler(void) {
      HAL_DMA_IRQHandler(hDcmiHandler.DMA_Handle);
  }
}
/** @addtogroup STM32F7xx_HAL_Examples
 * @{
 */

/** @addtogroup Templates
 * @{
 */

/* Private typedef -----------------------------------------------------------*/

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart6;
DMA_HandleTypeDef hdma_dcmi;
DMA2D_HandleTypeDef hdma2d;


#ifdef __GNUC__
	#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
	#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void Error_Handler(void);
static void CPU_CACHE_Enable(void);
static void MX_USART1_UART_Init(void);
static void MX_USART6_UART_Init(void);
static void MX_DMA2D_Init(void);

/* Private functions ---------------------------------------------------------*/
#define NUM_IN_CH 2
#define NUM_OUT_CH 3
#define IMG_WIDTH 160
#define IMG_HEIGHT 120
#define CNN_IMG_SIZE 32
#define resolution RESOLUTION_R160x120

uint8_t camera_buffer[NUM_IN_CH*IMG_WIDTH*IMG_HEIGHT];
uint8_t resized_buffer[NUM_OUT_CH*CNN_IMG_SIZE*CNN_IMG_SIZE];
char lcd_output_string[50];
//LCD_DISCO_F746NG lcd;


/**
 * @brief  Main program
 * @param  None
 * @retval None
 */

void resize_rgb565in_rgb888out(uint8_t* camera_image, uint8_t* resize_image)
{
  // offset so that only the center part of rectangular image is selected for resizing
  int width_offset = ((IMG_WIDTH-IMG_HEIGHT)/2)*NUM_IN_CH;

  int yresize_ratio = (IMG_HEIGHT/CNN_IMG_SIZE)*NUM_IN_CH;
  int xresize_ratio = (IMG_WIDTH/CNN_IMG_SIZE)*NUM_IN_CH;
  int resize_ratio = (xresize_ratio<yresize_ratio)?xresize_ratio:yresize_ratio;

  for(int y=0; y<CNN_IMG_SIZE; y++) {
    for(int x=0; x<CNN_IMG_SIZE; x++) {
      int orig_img_loc = (y*IMG_WIDTH*resize_ratio + x*resize_ratio + width_offset);
      // correcting the image inversion here
      int out_img_loc = ((CNN_IMG_SIZE-1-y)*CNN_IMG_SIZE + (CNN_IMG_SIZE-1-x))*NUM_OUT_CH;
      uint8_t pix_lo = camera_image[orig_img_loc];
      uint8_t pix_hi = camera_image[orig_img_loc+1];
      // convert RGB565 to RGB888
      resize_image[out_img_loc] = (0xF8 & pix_hi);
      resize_image[out_img_loc+1] = ((0x07 & pix_hi)<<5) | ((0xE0 & pix_lo)>>3);
      resize_image[out_img_loc+2] = (0x1F & pix_lo) << 3;
    }
  }
}

void display_image_rgb888(int x_dim, int y_dim, uint8_t* image_data)
{
  for(int y=0; y<y_dim; y++) {
    for(int x=0; x<x_dim; x++) {
      int pix_loc = (y*x_dim + x)*3;
      uint8_t a = 0xFF;
      uint8_t r = image_data[pix_loc];
      uint8_t g = image_data[pix_loc+1];
      uint8_t b = image_data[pix_loc+2];
      int pixel = a<<24 | r<<16 | g<<8 | b;
      BSP_LCD_DrawPixel(300+x, 100+y, pixel);
    }
  }
}


void display_image_rgb565(int x_dim, int y_dim, uint8_t* image_data)
{
  for(int y=0; y<y_dim; y++) {
    for(int x=0; x<x_dim; x++) {
      int pix_loc = (y*x_dim + x)*2;
      uint8_t a = 0xFF;
      uint8_t pix_lo = image_data[pix_loc];
      uint8_t pix_hi = image_data[pix_loc+1];
      uint8_t r = (0xF8 & pix_hi);
      uint8_t g = ((0x07 & pix_hi)<<5) | ((0xE0 & pix_lo)>>3);
      uint8_t b = (0x1F & pix_lo) << 3;
      int pixel = a<<24 | r<<16 | g<<8 | b;
      // inverted image, so draw from bottom-right to top-left
      BSP_LCD_DrawPixel(200-x, 160-y, pixel);
    }
  }
}

int main(void)
{

 CPU_CACHE_Enable();
 SystemClock_Config();
 MX_USART1_UART_Init();
 MX_USART6_UART_Init();
 MX_DMA2D_Init();


 char msg2[50] = "Camera Init - SUCCESS\r\n";
 uint16_t length2 = strlen(msg2);

 char msg3[50] = "Camera Init - FAILED\r\n";
 uint16_t length3 = strlen(msg3);
 uint32_t Im_size = 0;
 Im_size =  0x2580;

 /*
 BSP_LCD_Init();
 BSP_LCD_LayerDefaultInit(0, LCD_FB_START_ADDRESS);
 BSP_LCD_DisplayOn();
 BSP_LCD_SelectLayer(0);
 BSP_LCD_Clear(LCD_COLOR_WHITE);
 */



 BSP_LCD_Init();
 BSP_LCD_LayerDefaultInit(0, LCD_FB_START_ADDRESS);
 BSP_LCD_LayerDefaultInit(1, LCD_FB_START_ADDRESS+(BSP_LCD_GetXSize()*BSP_LCD_GetYSize()*4));
 BSP_LCD_DisplayOn();

 BSP_LCD_SelectLayer(0);
 BSP_LCD_Clear(LCD_COLOR_BLACK);

 BSP_LCD_SelectLayer(1);
 BSP_LCD_Clear(LCD_COLOR_BLACK);

 BSP_LCD_SetFont(&LCD_DEFAULT_FONT);
 BSP_LCD_SetBackColor(LCD_COLOR_WHITE);
 BSP_LCD_SetTextColor(LCD_COLOR_RED);


 BSP_LCD_Clear(LCD_COLOR_WHITE);

 HAL_Init();

 HAL_Delay(100);
 if( BSP_CAMERA_Init(RESOLUTION_R160x120) == CAMERA_OK ) {
	 HAL_UART_Transmit(&huart6, (uint8_t *)msg2, length2, 0xFFFF);
   } else {
	   HAL_UART_Transmit(&huart6, (uint8_t *)msg3, length3, 0xFFFF);
       BSP_LCD_Clear(LCD_COLOR_RED);
   }
   HAL_Delay(100);

   //HAL_UART_Transmit(&huart6, (uint8_t *)msg2, length2, 0xFFFF);


 while (1)
 {

  BSP_CAMERA_SnapshotStart(camera_buffer);
  resize_rgb565in_rgb888out(camera_buffer, resized_buffer);
  display_image_rgb888(CNN_IMG_SIZE, CNN_IMG_SIZE, resized_buffer);
  display_image_rgb565(IMG_WIDTH, IMG_HEIGHT, camera_buffer);
  //lcd.DisplayStringAt(0, LINE(8), (uint8_t *)msg4, CENTER_MODE);
 //HAL_DCMI_Start_DMA(&hDcmiHandler, DCMI_MODE_CONTINUOUS , (uint32_t)camera_buffer, Im_size);
 BSP_LCD_DisplayStringAt(0, LINE(8), (uint8_t *)"Original vs. scaled images", CENTER_MODE);

 }
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 12;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_6) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART6;
  PeriphClkInitStruct.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInitStruct.Usart6ClockSelection = RCC_USART6CLKSOURCE_PCLK2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

PUTCHAR_PROTOTYPE {
        HAL_UART_Transmit(&huart6, (uint8_t *)&ch, 1, 0xFFFF);

 return ch;
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
static void Error_Handler(void)
{
 /* User may add here some code to deal with this error */
 while(1)
 {
 }
}

/**
 * @brief  CPU L1-Cache enable.
 * @param  None
 * @retval None
 */
static void CPU_CACHE_Enable(void)
{
 /* Enable I-Cache */
 SCB_EnableICache();

 /* Enable D-Cache */
 SCB_EnableDCache();
}
/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;

  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }

}

/**
  * @brief USART6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART6_UART_Init(void)
{

  /* USER CODE BEGIN USART6_Init 0 */

  /* USER CODE END USART6_Init 0 */

  /* USER CODE BEGIN USART6_Init 1 */

  /* USER CODE END USART6_Init 1 */
  huart6.Instance = USART6;
  huart6.Init.BaudRate = 115200;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
  huart6.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart6.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart6) != HAL_OK)
  {
    Error_Handler();
  }
}


void HAL_UART_MspInit(UART_HandleTypeDef* huart)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(huart->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspInit 0 */

  /* USER CODE END USART1_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_USART1_CLK_ENABLE();

    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**USART1 GPIO Configuration
    PB7     ------> USART1_RX
    PA9     ------> USART1_TX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_9;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* USER CODE BEGIN USART1_MspInit 1 */

  /* USER CODE END USART1_MspInit 1 */
  }
  else if(huart->Instance==USART6)
  {
  /* USER CODE BEGIN USART6_MspInit 0 */

  /* USER CODE END USART6_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_USART6_CLK_ENABLE();

    __HAL_RCC_GPIOC_CLK_ENABLE();
    /**USART6 GPIO Configuration
    PC7     ------> USART6_RX
    PC6     ------> USART6_TX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_6;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /* USER CODE BEGIN USART6_MspInit 1 */

  /* USER CODE END USART6_MspInit 1 */
  }

}


static void MX_DMA2D_Init(void)
{

  /* USER CODE BEGIN DMA2D_Init 0 */

  /* USER CODE END DMA2D_Init 0 */

  /* USER CODE BEGIN DMA2D_Init 1 */

  /* USER CODE END DMA2D_Init 1 */
  hdma2d.Instance = DMA2D;
  hdma2d.Init.Mode = DMA2D_M2M;
  hdma2d.Init.ColorMode = DMA2D_OUTPUT_ARGB8888;
  hdma2d.Init.OutputOffset = 0;
  hdma2d.LayerCfg[1].InputOffset = 0;
  hdma2d.LayerCfg[1].InputColorMode = DMA2D_INPUT_ARGB8888;
  hdma2d.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
  hdma2d.LayerCfg[1].InputAlpha = 0;
  if (HAL_DMA2D_Init(&hdma2d) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_DMA2D_ConfigLayer(&hdma2d, 1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DMA2D_Init 2 */

  /* USER CODE END DMA2D_Init 2 */

}


void HAL_DMA2D_MspInit(DMA2D_HandleTypeDef* hdma2d)
{
  if(hdma2d->Instance==DMA2D)
  {
  /* USER CODE BEGIN DMA2D_MspInit 0 */

  /* USER CODE END DMA2D_MspInit 0 */
    /* Peripheral clock enable */
    __HAL_RCC_DMA2D_CLK_ENABLE();
  /* USER CODE BEGIN DMA2D_MspInit 1 */

  /* USER CODE END DMA2D_MspInit 1 */
  }

}




#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
 /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

 /* Infinite loop */
 while (1)
 {
 }
}
#endif

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
