
# Image recognition and classification using CNN implemented on Arm Cortex-M microcontroller

The objective of this project is to implement an object/image recogtion algorithm on an edge device (e.g. microcontroller) and to evaluate it's performance. 

This readme file will be updated to take account of the mbed and CubeMx projects files.

## Getting Started

These guidelines show how to setup the environment, quantize the pre-built cifar-10 model for the image recognition, deploying and running of the code.

### Prerequisites

### Hardware:

* STM32F746G-DISCO board
* STM32F4DID-Camera module

### Software:

* [Ubuntu 16.04](http://hr.releases.ubuntu.com/16.04.6/ubuntu-16.04.6-desktop-amd64.iso) - Linux OS version used
* [Pythin 2.7.12](https://www.python.org/downloads/release/python-2712/) - Python version used and can be skipped since it comes pre-installed on ubuntu 16.04
* [Caffe](https://caffe.berkeleyvision.org/) - Deep learning framework used
* [GNU Arm Toolchain](http://bit.ly/gnu-arm-embedded) - Version of compiler used



### Installation

These steps show how to setup the environment:

* [Ubuntu 16.04](https://tutorials.ubuntu.com/tutorial/tutorial-install-ubuntu-desktop-1604#0) - Complete installation guide. This link shows well detailed guide on how to install and run ubuntu 16.04
* [Caffe](https://caffe.berkeleyvision.org/install_apt.html) - This is caffe framework installation and compillation procedure. Ubuntu < 17.04 guide instllation instruction must be followed.
* [GNU Arm Toolchain](https://gnu-mcu-eclipse.github.io/toolchain/arm/install/) - Link to GNU GCC compiler installation guide

```
Once the the above steps have been completed, proceed with steps below via ubuntu terminal:

sudo apt install python-pip
sudo pip install mbed-cli
sudo pip install jsonschema
sudo pip install pyelftools
sudo apt install mercurial
sudo apt install git
```

### Clone project from Bitbucker repository

```
git clone https://youngolutosin@bitbucket.org/research-iot/cmsis_nn_stm32f746g.git
```

## Compiling code

From terminal navigate to cmsisnn_demo in the cloned directory as shown below:
```
cd research-iot-cmsis_nn_stm32f746g-d38ff489cf46
cd CMSISNN_Webinar
cd cmsisnn_demo
```
 and run the code as written below:

```
mbed compile -m DISCO_F746NG -t GCC_ARM --source .  --source ../ML-examples/cmsisnn-cifar10/code/m7 --source ../ML-examples/cmsisnn-cifar10/camera_demo/camera_app/ 
--source ../CMSIS_5/CMSIS/NN/Include --source ../CMSIS_5/CMSIS/NN/Source --source ../CMSIS_5/CMSIS/DSP/Include --source ../CMSIS_5/CMSIS/Core/Include -j8 
```

### Result of code compilation

The compilation result would generate a cmsisnn_demo.bin file which would be stored in the path specified below:
``` 
CMSISNN_webinar/cmsisnn_demo/BUILD/DISCO_F746NG/GCC-ARM
```

## Deployment

```
To run the code on STM32F746NG, plug the microcontroller to your system, copy and paste the cmsisnn_demo.bin file generated unto the microcontroller. This resets
the microcontroller automatically and runs the application. Aim the camera towards the object and the detected object would be captured on screen.
```
### Note
```
The cifar-10 model used has 10 classes (airplane, automobile, bird, cat, deer, dog, frog, horse, ship and truck) which means it can only recognize any object
of these classes.
```

## Author and supervisor

* **Young Olutosin** - supervised by **Mairo Leier**.

## Acknowledgment

```
This project was implemented based on the starter project published by STMICROELECTRONICS using CMSISNN. However, alot of modifications were made in other to get the project 
running. All the files included in this project were the modified files.

```



